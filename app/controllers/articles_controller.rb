class ArticlesController < ApplicationController
	before_action :authenticate_user!, except: [:show,:index]
	before_action :set_article, except: [:index,:new,:create]
	# before_action :validate_user, except: [:show]
	# GET /articles
	def index
		#Obtiene todos los registros SELECT * FROM articles
		@articles = Article.all
	end

	# GET /articles/:id
	def show
		@article.update_visits_count
		@comment = Comment.new
		#Where
		#Article.where.not("id = ? AND title = ?",params[:id],params[:title])
	end

	# GET /articles/new
	def new
		@article = Article.new
	end

	def edit
		
	end

	# POST /articles
	def create
		#INSERT INTO
		@article = current_user.articles.new(article_params)

		#@article.valid?
		if @article.save
			redirect_to @article
		else
			render :new
		end
	end

	# DELETE /articles/:id
	def destroy
		# DELETE FROM articles
		@article.destroy # Destroy elimina el objeto de la base de datos
		redirect_to articles_path
	end

	# PUT /articles/:id
	def update
		# UPDATE
		# @article.update_attributes({title: 'Nuevo titulo'})
		if @article.update(article_params)
			redirect_to @article
		else
			render :edit
		end
	end

	private

	def set_article
		#Encuentra los registros por id
		@article = Article.find(params[:id])
	end

	# def validate_user
	# 	redirect_to new_user_session_path, notice: "Necesitas iniciar sesion"
	# end

	def article_params
		params.require(:article).permit(:title,:body)
	end
end
